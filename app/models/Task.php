<?php namespace Nmax\Models;

use Nmax\Core\Model;

class Task extends Model
{
	protected $table = 'tasks';
	public $timestamps = false;
	public static $sortable = ['username', 'email', 'status'];
}