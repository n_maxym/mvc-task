<?php namespace Nmax\Core;

use Nmax\Core\View;

class Controller
{
	/**
	 * __construct
	 */
	public function __construct() {
		$this->view = new View();
	}

	/**
	 * getOffset
	 */
	public function getOffset() {
		return isset($_GET['page'])
			? (((int)$_GET['page'] - 1) * $this->perPage)
			: 0;
	}
}