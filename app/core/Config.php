<?php
namespace Nmax\Core;

class Config
{
	/**
	 * get
	 */
	public static function get(string $key) {
		$config = self::getConfig();
		return $config[$key] ?? null;
	}

	/**
	 * getConfig
	 */
	private static function getConfig(): array
	{
		$path = __DIR__ . "/../config.php";
		return (file_exists($path))
			? include $path
			: [];
	}
}