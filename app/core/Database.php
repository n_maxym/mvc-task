<?php
namespace Nmax\Core;

class Database
{
	/**
	 * __construct
	 */
	public function __construct() {
		$_dbsettings = parse_url(Config::get('DB_URL'));
		$host = isset($_dbsettings["port"])
			? $_dbsettings["host"] . ':' . $_dbsettings["port"]
			: $_dbsettings["host"];

		$dbSettings = [
		    'driver' => 'mysql',
		    'host' => $host,
		    'database' => trim($_dbsettings["path"],"/"),
		    'username' => $_dbsettings["user"],
		    'password' => $_dbsettings["pass"],
		    'collation' => 'utf8',
		    'prefix' => ''
		];

		$connect = new \Illuminate\Database\Capsule\Manager;
		$connect->addConnection($dbSettings);
		$connect->bootEloquent();
	}
}