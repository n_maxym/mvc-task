<?php
namespace Nmax\Core;

class View
{
	private $viewPath = '../views';
	private $layout = 'default';
	private $vars = [];

	/**
	 * layout
	 */
	public function layout(string $layout): View {
		file_exists(__DIR__ . "/{$this->viewPath}/layouts/{$layout}.php") or die('Invalid layout');

		$this->layout = $layout;
		return $this;
	}

	/**
	 * template
	 */
	public function template(string $template): View {
		file_exists(__DIR__ . "/{$this->viewPath}/templates/{$template}.php") or die('Invalid template');

		$this->template = $template;
		return $this;
	}

	/**
	 * vars
	 */
	public function vars(array $data): View {
		$this->vars = $data;
		return $this;
	}

	/**
	 * render
	 */
	public function render() {
		extract($this->vars);

		require_once(__DIR__ . "/{$this->viewPath}/layouts/{$this->layout}/header.php");
		require_once(__DIR__ . "/{$this->viewPath}/templates/{$this->template}.php");
		require_once(__DIR__ . "/{$this->viewPath}/layouts/{$this->layout}/footer.php");
	}
}