<?php
namespace Nmax\Core;

class Router
{
	/**
	 * __construct
	 */
	public function __construct() {
		return $this->route();
	}

	/**
	 * route
	 */
	private function route() {
		$dispatcher = \FastRoute\SimpleDispatcher(function(\FastRoute\RouteCollector $route) {
			include(__DIR__ . "/../routes.php");
		});

		// Fetch method and URI from somewhere
		$httpMethod = $_SERVER['REQUEST_METHOD'];
		$uri = $_SERVER['REQUEST_URI'];

		// Strip query string (?foo=bar) and decode URI
		if (false !== $pos = strpos($uri, '?')) {
		    $uri = substr($uri, 0, $pos);
		}
		$uri = rawurldecode($uri);

		$routeInfo = $dispatcher->dispatch($httpMethod, $uri);
		switch ($routeInfo[0]) {
		    case \FastRoute\Dispatcher::NOT_FOUND:
		        die('404');
		        break;
		    case \FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
		        $allowedMethods = $routeInfo[1];
		        die('405');
		        break;
		    case \FastRoute\Dispatcher::FOUND:
		    	//if($httpMethod == 'POST') set_cors_headers(); 
		    	
		        $handler = explode('@', $routeInfo[1]);
		        $vars = $routeInfo[2];
		        
		        $controller = new $handler[0]();
		        call_user_func_array(array($controller, $handler[1]), $vars);
		                
		        break;
		}
	}
}