<?php namespace Nmax\Core;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Model extends Eloquent
{
	/**
	 * paginatePagesCount
	 */
	public static function paginatePagesCount($perPage) {
		$count = self::count();
		return ceil($count / $perPage);
	}

	/**
	 * paginate
	 */
	public static function paginate($perPage) {
		$pagesCount = self::paginatePagesCount($perPage);

		return function() use ($pagesCount) { ?>
			<ul class="pagination">
			<?php for($i = 1; $i <= $pagesCount; $i++) : 
				$params = http_build_query(array_merge($_GET, array('page' => $i)));
			?>
				<li><a href="?<?= $params ?>" disabled="<?= $disabled ?>"><?= $i ?></a></li>
				<?php endfor ?>
			</ul>
		<?php };
	}
}