<?php

require_once(__DIR__ . "/../vendor/autoload.php");

use Nmax\Core\{Router, Database};

new Database();
new Router();