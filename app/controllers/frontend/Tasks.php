<?php namespace Nmax\Controllers\Frontend;

use Nmax\Core\Controller;
use Nmax\Core\Image;
use Nmax\Models\Task;

class Tasks extends Controller
{
	protected $perPage = 3;

	/**
	 * list
	 */
	public function list() {
		$tasksQuery = Task::offset($this->getOffset())
			->limit($this->perPage);

		$tasksQuery = (isset($_GET['sort_by']) && in_array($_GET['sort_by'], Task::$sortable))
			? $tasksQuery->orderBy($_GET['sort_by'], 'asc')
			: $tasksQuery;

		$data = [
			'tasks' => $tasksQuery->get()->toArray(),
			'sortableFields' => Task::$sortable,
			'paginate' => Task::paginate($this->perPage)
		];

		return $this->view
			->template('frontend/tasks')
			->vars($data)
			->render();
	}

	/**
	 * create
	 */
	public function create() {
		return $this->view
			->template('frontend/taskCreate')
			->render();
	}

	/**
	 * save
	 */
	public function save() {
		$task =  new Task();

		$task->username = $_REQUEST['username'];
		$task->email = $_REQUEST['email'];
		$task->text = $_REQUEST['text'];
		$task->status = 1;

		$task->save();

		header("Location: /");
	}
}