<?php namespace Nmax\Controllers\Backend;

use Nmax\Core\Controller;
use Nmax\Models\Task;

class Tasks extends Controller
{
	/**
	 * list
	 */
	public function list() {
		$data = [
			'tasks' => Task::get()->toArray(),
		];

		return $this->view
			->template('backend/tasks')
			->vars($data)
			->render();
	}

	/**
	 * edit
	 */
	public function edit($id) {
		$data = [
			'task' => Task::find($id)->toArray()
		];

		return $this->view
			->template('backend/taskEdit')
			->vars($data)
			->render();
	}

	/**
	 * save
	 */
	public function save($id) {
		$task = Task::findOrFail($id);

		$task->text = $_REQUEST['text'];
		$task->status = isset($_REQUEST['status']) ? 1 : 0;

		$task->save();

		header("Location: /admin/tasks");
	}
}