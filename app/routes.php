<?php
	
// FRONTEND
$route->addRoute('GET', '/', 'Nmax\Controllers\Frontend\Tasks@list');
$route->addRoute('GET', '/create', 'Nmax\Controllers\Frontend\Tasks@create');
$route->addRoute('POST', '/save', 'Nmax\Controllers\Frontend\Tasks@save');

// BACKEND
$route->addRoute('GET', '/admin/tasks', 'Nmax\Controllers\Backend\Tasks@list');
$route->addRoute('GET', '/admin/tasks/edit/{id:\d+}', 'Nmax\Controllers\Backend\Tasks@edit');
$route->addRoute('POST', '/admin/tasks/save/{id:\d+}', 'Nmax\Controllers\Backend\Tasks@save');
