<div class="jumbotron">
	<h1>MVC Example</h1>
</div>

<div class="page-header">
	<h1>Tasks</h1>
</div>

<div class="row">
	<div class="col-md-2">
		<strong>Sort By</strong>
		<form id="sort" action="/" method="get">
			<select class="form-control" name="sort_by">
				<?php foreach($sortableFields as $row) : ?>
					<option <?php if($_GET['sort_by'] == $row) : ?> selected="" <?php endif ?>>
						<?= $row ?>
					</option>
				<?php endforeach ?>
			</select>
		</form>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
	  <table class="table table-striped">
	    <thead>
	      <tr>
	        <th>#</th>
	        <th>Username</th>
	        <th>Email</th>
	        <th>Text</th>
	        <th>Status</th>
	      </tr>
	    </thead>
	    <tbody>
	      <?php foreach($tasks as $key => $row) : ?>
	      <tr>
	        <td><?= $row['id'] ?></td>
	        <td><?= $row['username'] ?></td>
	        <td><?= $row['email'] ?></td>
	        <td><?= $row['text'] ?></td>
	        <td><?= $row['status'] ?></td>
	      </tr>
	      <?php endforeach ?>
	    </tbody>
	  </table>
	</div>
</div>

<?= $paginate() ?>

<script type="text/javascript">
$('form#sort').on('change', function() {
	$(this).submit();
});
</script>