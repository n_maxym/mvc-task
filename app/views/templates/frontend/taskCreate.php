<div class="jumbotron">
	<h1>Add New Task</h1>
</div>

<form action="/save" method="post">
	<div class="form-group">
		<label for="username">Username</label>
		<input id="username" class="form-control" name="username">
	</div>
	<div class="form-group">
		<label for="email">Email</label>
		<input id="email" class="form-control" name="email">
	</div>
	<div class="form-group">
		<label for="text">Text</label>
		<textarea id="text" class="form-control" rows="10" name="text"></textarea>
	</div>
	<div class="form-group">
      	<button type="submit" class="btn btn-default">SAVE</button>
  	</div>
</form>