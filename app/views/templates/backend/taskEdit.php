<div class="jumbotron">
	<h1>Edit Task</h1>
</div>

<form action="/admin/tasks/save/<?= $task['id'] ?>" method="post">
	<div class="checkbox">
		<label>
			<input type="checkbox" <?php if($task['status']) : ?>checked=""<?php endif ?> name="status"> Status
		</label>
	</div>
	<div class="form-group">
		<label for="username">Username</label>
		<input id="username" class="form-control" name="username" value="<?= $task['username'] ?>" disabled>
	</div>
	<div class="form-group">
		<label for="email">Email</label>
		<input id="email" class="form-control" name="email" value="<?= $task['email'] ?>" disabled>
	</div>
	<div class="form-group">
		<label for="text">Text</label>
		<textarea id="text" class="form-control" rows="10" name="text"><?= $task['text'] ?></textarea>
	</div>
	<div class="form-group">
      	<button type="submit" class="btn btn-default">SAVE</button>
  	</div>
</form>
