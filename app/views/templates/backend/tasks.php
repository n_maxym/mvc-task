<div class="jumbotron">
	<h1>Edit Tasks</h1>
</div>

<div class="row">
	<div class="col-md-12">
	  <table class="table table-striped">
	    <thead>
	      <tr>
	        <th>#</th>
	        <th>Username</th>
	        <th>Email</th>
	        <th>Text</th>
	        <th>Status</th>
	        <th>&nbsp;</th>
	      </tr>
	    </thead>
	    <tbody>
	      <?php foreach($tasks as $key => $row) : ?>
	      <tr>
	        <td><?= $row['id'] ?></td>
	        <td><?= $row['username'] ?></td>
	        <td><?= $row['email'] ?></td>
	        <td><?= $row['text'] ?></td>
	        <td><?= $row['status'] ?></td>
	        <td><a href="/admin/tasks/edit/<?= $row['id'] ?>">edit</a></td>
	      </tr>
	      <?php endforeach ?>
	    </tbody>
	  </table>
	</div>
</div>
